package user;

import org.apache.commons.codec.digest.DigestUtils;

import javax.jws.soap.SOAPBinding;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        UserStorage userStorage = new UserStorage();

        Scanner scanner = new Scanner(System.in);

        System.out.println("Introdu user:");
        String username = scanner.nextLine();

        System.out.println("Introdu parola:");
        String password = scanner.nextLine();

        User user = userStorage.getByUsername(username);
        if(user != null && user.getPassword().equals(DigestUtils.md5Hex(password))) {
            System.out.println("LOGIN SUCCESS");
        } else {
            System.out.println("INVALID CREDENTIALS");
        }

    }
}
