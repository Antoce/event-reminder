import org.junit.Assert;
import org.junit.Test;
import user.User;
import user.UserStorage;

import java.io.FileNotFoundException;

public class UserStorageTest {


    @Test
    public void testGetByUsername() throws FileNotFoundException {
        UserStorage userStorage = new UserStorage();
        User user = userStorage.getByUsername("test");

        Assert.assertNotNull(user);
        Assert.assertEquals(user.getPassword(),"parola2");
    }
}
